async def parse_json(request):
    return await request.json()


async def parse_form(request):
    return await request.post()
