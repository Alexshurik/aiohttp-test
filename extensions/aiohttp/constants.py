from aiohttp import hdrs


SAFE_METHODS = (hdrs.METH_GET, hdrs.METH_HEAD, hdrs.METH_OPTIONS)
