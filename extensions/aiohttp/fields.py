class ImageField:
    """
    Простая обертка над классом FileField
    с возможностью добавить новое поле "image" для картинки
    """
    # ToDo: можно покруче сделать
    def __init__(self, file_field, image):
        self.name = file_field.name
        self.filename = file_field.filename
        self.file = file_field.file
        self.content_type = file_field.content_type
        self.headers = file_field.headers
        self.image = image
