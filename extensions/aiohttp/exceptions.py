from extensions.aiohttp import status


class APIException(Exception):
    status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
    default_msg = 'Ошибка на сервере'

    def __init__(self, msg=None):
        self.msg = msg or self.default_msg


class ValidationError(APIException):
    status_code = status.HTTP_400_BAD_REQUEST
    default_msg = 'Неверные введенные данные'


class ParseError(APIException):
    status_code = status.HTTP_400_BAD_REQUEST
    default_msg = 'Невозможно распарсить тело запроса'


class AuthenticationFailed(APIException):
    status_code = status.HTTP_401_UNAUTHORIZED
    default_msg = 'Неверные данные аутентификации'


class NotAuthenticated(APIException):
    status_code = status.HTTP_401_UNAUTHORIZED
    default_msg = 'Данные для аутентификации не были предоставлены'


class PermissionDenied(APIException):
    status_code = status.HTTP_403_FORBIDDEN
    default_msg = 'У вас не достаточно прав для данного действия'


class UnsupportedMediaType(APIException):
    status_code = status.HTTP_415_UNSUPPORTED_MEDIA_TYPE
    default_msg = 'Media type {media_type} не поддерживается'

    def __init__(self, media_type, msg=None):
        if msg is None:
            msg = self.default_msg.format(media_type=media_type)
        super().__init__(msg)
