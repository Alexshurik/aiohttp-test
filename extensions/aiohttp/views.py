from aiohttp import web, hdrs
from marshmallow import ValidationError as MarshmallowValidationError

from extensions.aiohttp.constants import SAFE_METHODS
from extensions.aiohttp.exceptions import APIException, ParseError, UnsupportedMediaType
from extensions.aiohttp.parsers import parse_json, parse_form


class APIView(web.View):
    def get_parser(self):
        if self.request.content_type == 'application/json':
            return parse_json
        elif self.request.content_type in ('application/x-www-form-urlencoded', 'multipart/form-data'):
            return parse_form
        # ToDo: добавить другие media-types
        # Н-р, Raw File Upload

    async def parse_request(self):
        if self.request._method not in SAFE_METHODS and self.request.can_read_body:
            parser = self.get_parser()
            if not parser:
                raise UnsupportedMediaType(self.request.content_type)

            try:
                self.request.data = await parser(self.request)
            except Exception:
                raise ParseError()

    def handle_exception(self, exc):
        if isinstance(exc, MarshmallowValidationError):
            msg = exc.messages
            status_code = 400
        elif isinstance(exc, APIException):
            msg = exc.msg
            status_code = exc.status_code
        else:
            raise exc

        if isinstance(msg, dict):
            response_data = msg
        else:
            response_data = {
                'error': msg
            }
        return web.json_response(response_data, status=status_code)

    async def _iter(self):
        if self.request.method not in hdrs.METH_ALL:
            self._raise_allowed_methods()

        await self.parse_request()

        method = getattr(self, self.request.method.lower(), None)
        if method is None:
            self._raise_allowed_methods()

        try:
            resp = await method()
        except Exception as exc:
            resp = self.handle_exception(exc)
        return resp
