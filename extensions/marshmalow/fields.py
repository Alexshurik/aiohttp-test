import os
from io import BytesIO

from marshmallow.fields import Field, Integer as MarshMallowInteger,\
    Float as MarshMallowFloat, Decimal as MarshMallowDecimal
from extensions.aiohttp.fields import ImageField as RequestImageField


class FileField(Field):
    default_error_messages = {
        'required': 'No file was submitted.',
        'invalid': 'The submitted data was not a file. Check the encoding type on the form.',
        'no_name': 'No filename could be determined.',
        'empty': 'The submitted file is empty.',
        'max_length': 'Ensure this filename has at most {max_length} characters (it has {length}).',
    }

    def __init__(self, allow_empty_file=False, max_length=False, *args, **kwargs):
        self.allow_empty_file = allow_empty_file
        self.max_length = max_length
        super().__init__(*args, **kwargs)

    def _deserialize(self, value, attr, data):
        try:
            # `FileField` objects should have name and file attributes.
            file_name = value.name
            file = value.file
            
            old_file_position = file.tell()
            file.seek(0, os.SEEK_END)
            
            file_size = value.file.tell()
            value.file.seek(old_file_position, os.SEEK_SET)
        except Exception:
            self.fail('invalid')

        if not file_name:
            self.fail('no_name')
        if not self.allow_empty_file and not file_size:
            self.fail('empty')
        if self.max_length and len(file_name) > self.max_length:
            self.fail('max_length', max_length=self.max_length, length=len(file_name))
        return value

    def _serialize(self, value, attr, obj):
        if not value:
            return None
        # ToDo: добавить в будущем url-отображение, как в drf
        return value.name


class ImageField(FileField):
    default_error_messages = {
        'invalid_image':
            'Upload a valid image. The file you uploaded was either not an image or a corrupted image.',
    }

    def _deserialize(self, value, attr, data):
        file_object = super()._deserialize(value, attr, data)

        if file_object is None:
            return None

        from PIL import Image

        # We need to get a file object for Pillow. We might have a path or we might
        # have to read the data into memory.
        file = BytesIO(file_object.file.read())
        try:
            # load() could spot a truncated JPEG, but it loads the entire
            # image in memory, which is a DoS vector. See #3848 and #18520.
            image = Image.open(file)
            # verify() must be called immediately after the constructor.
            image.verify()

            image_field = RequestImageField(file_object, image)
        except Exception:
            # Pillow doesn't recognize it as an image.
            self.fail('invalid_image')
        image_field.file.seek(0)
        return image_field


class MinMaxNumberMixin:
    default_error_messages = {
        'too_small': 'Should be greater than or equal to {min_value}',
        'too_big': 'Should be less than or equal to {max_value}',
    }

    def __init__(self, min_value=None, max_value=None, *args, **kwargs):
        self.min_value = min_value
        self.max_value = max_value
        super().__init__(*args, **kwargs)

    def _validated(self, value):
        value = super()._validated(value)
    
        if self.min_value is not None and value < self.min_value:
            self.fail('too_small', min_value=self.min_value)
        if self.max_value and value > self.max_value:
            self.fail('too_big', max_value=self.max_value)
        return value


class Integer(MinMaxNumberMixin, MarshMallowInteger):
    pass


class Float(MinMaxNumberMixin, MarshMallowFloat):
    pass


class Decimal(MinMaxNumberMixin, MarshMallowDecimal):
    pass
