import asyncio
import concurrent
import io

import aiohttp
from PIL import Image
from aiohttp.web_response import Response

from extensions.aiohttp import status
from extensions.aiohttp.exceptions import APIException, ValidationError
from extensions.aiohttp.views import APIView
from images.constants import IMAGE_TOO_BIG_ERROR_TEXT
from images.helpers import is_image_has_valid_size
from images.lib import get_processed_image
from images.serializers import ImageSerializer


class ImageView(APIView):
    async def post(self):
        data = ImageSerializer().load(self.request.data)

        if data['img_url']:
            timeout = aiohttp.ClientTimeout(total=0.01)
            async with aiohttp.ClientSession(timeout=timeout) as session:
                try:
                    async with session.get(data['img_url']) as response:
                        if response.status == status.HTTP_404_NOT_FOUND:
                            raise APIException('Картинка не найдена')
                        elif response.status != status.HTTP_200_OK:
                            raise APIException('Невозможно скачать картинку')
                        image = await response.read()

                        ImageSerializer().load({'img': image}, partial=True)
                except asyncio.TimeoutError:
                    raise APIException('Невозможно скачать картинку: превышен предел ожидания')
        else:
            image = data['img'].file.read()

        result_image = await get_processed_image(
            image,
            data['common_style_id'],
            data['foreground_style_id'],
            data['common_style_opacity'],
            data['foreground_style_opacity'],
        )

        # ToDo: разобраться че там как content_type автоматом ставить
        return Response(body=result_image, content_type='image/jpeg')
