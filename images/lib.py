import aiohttp

from images.helpers import merge_images
from prisma.api import PrismaAsyncAPICaller


async def get_processed_image(
        source_image,
        common_style_id,
        foreground_style_id,
        common_style_opacity,
        foreground_style_opacity,
):
    """
    Processes source image
    :param source_image: bytes
    :param foreground_style_id: Prsima Style: str
    :param common_style_id: Prsima Style: str
    :param common_style_opacity: float
    :param foreground_style_opacity: float
    :return: processed image: bytes
    """

    async with aiohttp.ClientSession() as session:
        api = PrismaAsyncAPICaller(session)

        source_image_with_common_style = await api.get_styled_image(
            source_image,
            common_style_opacity,
            common_style_id,
        )

        source_image_with_foreground_style = await api.get_styled_image(
            source_image,
            foreground_style_opacity,
            foreground_style_id,
        )

        image_foreground = await api.get_image_foreground(source_image)

        return merge_images(
            source_image_with_common_style,
            source_image_with_foreground_style,
            image_foreground
        )
