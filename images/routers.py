from aiohttp import web

from images.views import ImageView


routers = [
    web.route('*', '/images/', ImageView)
]
