import io

from PIL import Image

import settings


def apply_transparency_mask(img: Image, mask: Image):
    """ Replace 32 shades of black with transparency. """
    mask_alpha = mask.getchannel('A')
    img.putalpha(mask_alpha)


def merge_images(_bg: bytes, _fg: bytes, _mask: bytes):
    """
    Load images from bytes.
    Turn black to transparent.
    Compose with respect to alpha and return.
    """
    bg = Image.open(io.BytesIO(_bg)).convert('RGBA')
    fg = Image.open(io.BytesIO(_fg)).convert('RGBA')
    mask = Image.open(io.BytesIO(_mask)).convert('RGBA')
    apply_transparency_mask(fg, mask)

    result_img = Image.alpha_composite(
        bg,
        fg
    )

    buf = io.BytesIO()
    result_img.convert('RGB').save(buf, 'JPEG')
    return buf.getvalue()


def is_image_has_valid_size(image):
    width, height = image.size
    return width <= settings.KABOOK_MAX_IMAGE_WIDTH and height <= settings.KABOOK_MAX_IMAGE_HEIGHT
