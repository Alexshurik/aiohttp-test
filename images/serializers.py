from marshmallow import Schema, fields, validates_schema, ValidationError, validates

import settings
from extensions.marshmalow.fields import ImageField, Float
from images.constants import IMAGE_TOO_BIG_ERROR_TEXT
from images.helpers import is_image_has_valid_size


class ImageSerializer(Schema):
    img = ImageField()
    foreground_style_id = fields.Str(
        missing=settings.PRISMA_DEFAULT_FOREGROUND_STYLE,
    )
    common_style_id = fields.Str(
        missing=settings.PRISMA_DEFAULT_COMMON_STYLE,
    )
    foreground_style_opacity = Float(
        missing=settings.PRISMA_DEFAULT_FOREGROUND_STYLE_OPACITY,
        min_value=0,
        max_value=100,
    )
    common_style_opacity = Float(
        missing=settings.PRISMA_DEFAULT_COMMON_STYLE_OPACITY,
        min_value=0,
        max_value=100,
    )
    img_url = fields.URL()

    @validates_schema
    def validate_schema(self, data):
        if 'img' in data and 'img_url' in data:
            raise ValidationError(
                'Only one of this field should be specified: ["img", "img_url"]',
                'error'
            )
        if 'img' not in data and 'img_url' not in data:
            raise ValidationError(
                'One of this field should be specified: ["img", "img_url"]',
                'error'
            )

    @validates('img')
    def validate_img(self, img):
        if not is_image_has_valid_size(img.image):
            raise ValidationError(IMAGE_TOO_BIG_ERROR_TEXT)
