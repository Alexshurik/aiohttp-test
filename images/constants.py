import settings

IMAGE_PROCESSING_AWAITING_START = 'IMAGE_PROCESSING_AWAITING_START'
IMAGE_PROCESSING_IN_PROGRESS = 'IMAGE_PROCESSING_IN_PROGRESS'
IMAGE_PROCESSING_DONE = 'IMAGE_PROCESSING_DONE'
IMAGE_PROCESSING_ERROR = 'IMAGE_PROCESSING_ERROR'

IMAGE_TOO_BIG_ERROR_TEXT = f"""\
Image too big. It should not exceed \
{settings.KABOOK_MAX_IMAGE_WIDTH}x{settings.KABOOK_MAX_IMAGE_HEIGHT}\
"""
