import argparse
import asyncio

import aiohttp_debugtoolbar
import uvloop
from aiohttp import web
from routers import setup_routes

import settings


asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())


def start_app(port):
    app = web.Application(client_max_size=settings.CLIENT_MAX_SIZE)
    setup_routes(app)
    web.run_app(app, port=port)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--port', help='Port where server will be serving')
    args = parser.parse_args()
    port = int(args.port) if args.port else 8080
    start_app(port)
