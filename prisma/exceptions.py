from extensions.aiohttp import status
from extensions.aiohttp.exceptions import APIException


class PrismaAPIException(APIException):
    status_code = status.HTTP_500_INTERNAL_SERVER_ERROR


class PrismaNotAuthorizedError(PrismaAPIException):
    default_detail = 'Unable to connect to PrismaAPI (wrong credentials)'


class SegmentationException(PrismaAPIException):
    default_detail = 'Background segmentation failed'


class StyleChangeException(PrismaAPIException):
    def __init__(self, style_id):
        detail = f'Can not apply style: "{style_id}" (prisma error)'
        super().__init__(detail)


class StyleNotFoundException(PrismaAPIException):
    status_code = status.HTTP_400_BAD_REQUEST

    def __init__(self, style_id):
        detail = f'Style "{style_id}" not found'
        super().__init__(detail)


class StylesGettingException(PrismaAPIException):
    default_detail = 'Unable to get style list (prisma error)'
