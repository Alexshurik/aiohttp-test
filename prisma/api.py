import settings
from extensions.aiohttp import status
from prisma.exceptions import StyleChangeException, SegmentationException, StyleNotFoundException, \
    StylesGettingException, PrismaNotAuthorizedError


class PrismaAsyncAPICaller:
    """
    Class providing methods for working with API

    All API calls are asynchronous
    """
    prisma_base_headers = {
        'prisma-partner-id': settings.PRISMA_PARTNER_ID,
        'prisma-partner-key': settings.PRISMA_PARTNER_KEY,
    }
    prisma_base_url = 'https://partner-api.neuralprisma.com'
    prisma_default_style = '228848f5-c1df-47ee-88e1-3011613a9812'

    def raise_on_401(self, response):
        resp_status = getattr(response, 'status', None) or getattr(response, 'status_code', None)
        if resp_status == status.HTTP_401_UNAUTHORIZED:
            raise PrismaNotAuthorizedError

    def __init__(self, session):
        """
        :param session: aiohttp ClientSession
        """
        self.session = session

    async def get_image_foreground(self, img):
        """
        Returns image without background

        This method uses Prisma's Portrait Segmentation (endpoint 3).

        :param img: bytes
        :return: image without background: bytes
        """
        async with self.session.post(
            f'{self.prisma_base_url}/process_segmentation',
            headers={
                **self.prisma_base_headers,
                'Content-Type': 'image/jpeg',
            },
            data=img
        ) as response:

            self.raise_on_401(response)
            if response.status != status.HTTP_200_OK:
                raise SegmentationException
            return await response.read()

    async def get_styled_image(self, img, opacity, style_id):
        """
        Returns image with applied style

        This method uses Prisma's Image Style Transfer (endpoint 2).

        :param img: bytes
        :param opacity: float
        :param style_id: Prisma style id: string
        :return: image with applied style: bytes
        """
        async with self.session.post(
            f'{self.prisma_base_url}/process/{style_id}',
            headers={
                **self.prisma_base_headers,
                'Content-Type': 'image/jpeg',
                'prisma-opacity': str(opacity),
            },
            data=img
        ) as response:

            self.raise_on_401(response)
            if response.status == status.HTTP_404_NOT_FOUND:
                raise StyleNotFoundException(style_id)
            elif response.status != status.HTTP_200_OK:
                raise StyleChangeException(style_id)
            return await response.read()

    async def get_styles(self):
        """
        Returns Prisma's styles

        :return: list of Prisma Style objects
        """
        async with self.session.get(
            f'{self.prisma_base_url}/styles',
            headers=self.prisma_base_headers
        ) as response:

            self.raise_on_401(response)
            if response.status != status.HTTP_200_OK:
                raise StylesGettingException
            return await response.json()
