from images.routers import routers as image_routers
from styles.routers import routers as styles_routers


def setup_routes(app):
    app.router.add_routes([
        *image_routers,
        *styles_routers,
    ]),

