from aiohttp import web

from styles.views import StyleListView


routers = [
    web.route('*', '/styles/', StyleListView)
]
