import aiohttp
from aiohttp import web

from extensions.aiohttp.views import APIView
from prisma.api import PrismaAsyncAPICaller


class StyleListView(APIView):
    async def get(self):
        async with aiohttp.ClientSession() as session:
            api = PrismaAsyncAPICaller(session)
            styles = await api.get_styles()
        return web.json_response(styles)
